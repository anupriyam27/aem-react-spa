import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';
import { FacebookShareButton, FacebookIcon, PinterestShareButton, PinterestIcon } from 'react-share';

const SharingEditConfig = {
    emptyLabel: 'Sharing',
    isEmpty: function (props) {
        // return !props || !props.text || props.text.trim().length < 1;
    }
};

class Sharing extends Component {
    render() {
        return (
            <div>
            <span>
            	<FacebookShareButton url={'https://www.facebook.com/'}>
            		<FacebookIcon iconFillColor='white' size={42} />
            	</FacebookShareButton>
            </span>
            &nbsp;
            <span>	
            	<PinterestShareButton url={'https://www.pinterest.com'}  media='#'>
        			<PinterestIcon iconFillColor='white' size={42} />
        		</PinterestShareButton>
        	</span>        	
            </div>
        );
    }
}

export default MapTo('wknd-spa-react/components/sharing')(
    Sharing,
    SharingEditConfig
);