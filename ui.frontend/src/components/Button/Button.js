import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';
import './Button.scss';

const ButtonEditConfig = {
    emptyLabel: 'Form Button',

    isEmpty: function (props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

class Button extends Component {
    render() {
        console.log('Button Props', this.props)
        return (
            <>
                <a class="cmp-button" href={this.props.link} aria-label="read our articles">
                    <span class="cmp-button__text">{this.props.text}</span>
                </a>
            </>
        )
    }
}

export default MapTo('wknd-spa-react/components/button')(
    Button,
    ButtonEditConfig
);