import React, { Component } from "react";
import { MapTo } from "@adobe/cq-react-editable-components";
import { Link } from "react-router-dom";
require("./List.scss");

const ListEditConfig = {
  emptyLabel: "List",

  isEmpty: function (props) {
    return !props || !props.items || props.items.length < 1;
  },
};

function ItemTitle(props) {
  return (
    <>
      <span className="ListItem-title">{props.title}</span>
      <span className="ListItem-date">{props.date}</span>
    </>
  );
}

class ListItem extends Component {
  get date() {
    if (!this.props.date) {
      return null;
    }
    let date = new Date(this.props.date);
    return date.toLocaleDateString("en-US");
  }

  get content() {
    if (this.props.link) {
      return (
        <Link className="ListItem-link" to={this.props.url}>
          {this.props.title}
          <span className="ListItem-date">{this.date}</span>
        </Link>
      );
    }
    return <ItemTitle title={this.props.title} date={this.date} />;
  }

  render() {
    if (!this.props.path || !this.props.title || !this.props.url) {
      return null;
    }
    return (
      <li className="ListItem" key={this.props.path}>
        {this.content}
        <span className="ListItem-description">{this.props.description}</span>
      </li>
    );
  }
}

class List extends Component {
  render() {
    return (
      <div className="List">
        <ul className="List-wrapper">
          {this.props.items &&
            this.props.items.map((listItem, index) => {
              return (
                <ListItem
                  path={listItem.path}
                  url={listItem.url}
                  title={listItem.title}
                  date={listItem.lastModified}
                  description={listItem.description}
                  link={this.props.linkItems}
                />
              );
            })}
        </ul>
      </div>
    );
  }
}

export default MapTo("wknd-spa-react/components/list")(
  List,
  ListEditConfig
);
