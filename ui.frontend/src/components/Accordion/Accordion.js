import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel } from 'react-accessible-accordion';
import DOMPurify from 'dompurify';
require('./Accordion.scss');

const AccordionsEditConfig = {
	    emptyLabel: 'Accordions',
	    isEmpty: function (props) {
	        return !props || !props.cqItems || props.cqItems.length < 1;
	      },
	};

class Accordions extends Component {
    render() {
        //console.log('Accordion Props - CQ Model', this.props);
        return (
            <div class="accordion aem-GridColumn--tablet--12 aem-GridColumn--phone--none aem-GridColumn--phone--12 aem-GridColumn aem-GridColumn--default--8 aem-GridColumn--offset--phone--0">
                {this.props.cqItemsOrder &&
                    this.props.cqItemsOrder.map((listItem, index) => {
                      return (
                        <AccordionsItems
                          item={this.props.cqItemsOrder[index]}
                          items={this.props.cqItems}
                        />
                      );
                    })}
            </div>
        );
    }
}

class AccordionsItems extends Component{
	render() {
        
        let accordion_Item = this.props.items[this.props.item];
        //console.log('accordion_Item Props - CQ Model', accordion_Item[':items']);
        if(accordion_Item[':items'].text != undefined){
        return (
        		<div>
        		<Accordion allowZeroExpanded={true}>
                <AccordionItem>
                    <AccordionItemHeading>
                        <AccordionItemButton>
                            <span class="cmp-accordion__title">{accordion_Item['cq:panelTitle']}</span>
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                        <p class="acc-text"
                        	data-rte-editelement
                            dangerouslySetInnerHTML={{
                                __html: DOMPurify.sanitize(accordion_Item[':items'].text['text'])
                            }}
                        />
                    </AccordionItemPanel>
                </AccordionItem>
                </Accordion>
                </div>
        );
        }
        else{
          	 return (
          			 <Accordion allowZeroExpanded={true}>
          	            <AccordionItem>
          	                <AccordionItemHeading>
          	                    <AccordionItemButton>
          	                        <span class="cmp-accordion__title">{accordion_Item['cq:panelTitle']}</span>
          	                    </AccordionItemButton>
          	                </AccordionItemHeading>
          	                <AccordionItemPanel>
          	                    <p/>
          	                </AccordionItemPanel>
          	            </AccordionItem>
          	            </Accordion>
               		
               );
          }
    }
}

export default MapTo('wknd-spa-react/components/accordion')(
	Accordions,
	AccordionsEditConfig
);