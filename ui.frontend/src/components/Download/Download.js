import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';
import DOMPurify from 'dompurify';

import './Download.scss';

const DownloadComEditConfig = {
	    emptyLabel: 'DownloadCom',
	    isEmpty: function (props) {
	    return !props || !props.url || props.url.trim().length < 1;
	    }
	};

class DownloadCom extends Component{
	render() {
        
    return (
    		
    		<div class="download aem-GridColumn aem-GridColumn--default--3">
    		<div class="cmp-download">
    		    <h3 class="cmp-download__title">
    		        <a class="cmp-download__title-link" href={this.props.url} target="_blank" download>
    		        		{this.props.title}
    		        </a>
    		    </h3>
    		    <div class="cmp-download__description"
    		    	data-rte-editelement
                    dangerouslySetInnerHTML={{
                        __html: DOMPurify.sanitize(this.props.description)
                    }}
    		    />
    		    <dl class="cmp-download__properties">
    		        <div class="cmp-download__property cmp-download__property--filename">
    		            <dt class="cmp-download__property-label">Filename</dt>
    		            <dd class="cmp-download__property-content">{this.props.filename}</dd>
    		        </div>
    		        <div class="cmp-download__property cmp-download__property--size">
    		            <dt class="cmp-download__property-label">Size</dt>
    		            <dd class="cmp-download__property-content">{this.props.size}</dd>
    		        </div>
    		        <div class="cmp-download__property cmp-download__property--format">
    		            <dt class="cmp-download__property-label">Format</dt>
    		            <dd class="cmp-download__property-content">{this.props.format}</dd>
    		        </div>
    		    </dl>
    		    <a class="cmp-download__action" href={this.props.url} target="_blank" download>
    		        <span class="cmp-download__action-text">{this.props.actionText}</span>
    		    </a>
    		</div>
    		</div>
    );
}
	
} 

export default MapTo('wknd-spa-react/components/download')(
	DownloadCom,
	DownloadComEditConfig
);