import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

const ContainerEditConfig = {
    emptyLabel: 'Container',

    isEmpty: function (props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

class Container extends Component {
    render() {
        console.log('Container Props', this.props);
        return (
            <>
                {/* <button className="cmp-form-button" id={this.props.id} type={this.props.type} name={this.props.name} value={this.props.value}>{this.props.title}</button> */}
            </>
        )
    }
}

export default MapTo('wknd-spa-react/components/form/container')(
    Container,
    ContainerEditConfig
);