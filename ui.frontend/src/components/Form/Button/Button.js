import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';
import './Button.scss';

const ButtonEditConfig = {
    emptyLabel: 'Form Button',

    isEmpty: function (props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

class Button extends Component {
    render() {
        return (
            <>
                <button className="cmp-form-button w250" id={this.props.id} type={this.props.type} name={this.props.name} value={this.props.value}>{this.props.title}</button>
            </>
        )
    }
}

export default MapTo('wknd-spa-react/components/form/button')(
    Button,
    ButtonEditConfig
);