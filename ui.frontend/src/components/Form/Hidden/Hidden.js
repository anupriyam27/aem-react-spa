import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

const HiddenEditConfig = {
    emptyLabel: 'Form Hidden',

    isEmpty: function (props) {
        return !props || !props.name || props.name.trim().length < 1;
    }
};

class Hidden extends Component {
    render() {
        return (
            <>
                <input type="hidden" id={this.props.id} name={this.props.name} value={this.props.value}></input>
            </>
        )
    }
}

export default MapTo('wknd-spa-react/components/form/hidden')(
    Hidden,
    HiddenEditConfig
);