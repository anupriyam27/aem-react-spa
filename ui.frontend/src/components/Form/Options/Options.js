import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

const OptionsEditConfig = {
    emptyLabel: 'Form Options',

    isEmpty: function (props) {
        return !props || !props.title || props.title.trim().length < 1;
    }
};

class Options extends Component {

    render() {
        // console.log('Form Hidden Props', this.props);
        let optionList = this.props.items.map(item =>
            <div className="mb5">
                <input className="" type={this.props.type === "CHECKBOX" ? "checkbox" : this.props.type === "RADIO" ? "radio" : ''} name={this.props.name} value={item.value}></input>
                <span className="">{item.text}</span>
            </div>
        )
        return (
            <>
                <fieldset className="w225 mb20">
                    {(this.props.type !== "DROP_DOWN" && this.props.type !== "MULTI_DROP_DOWN") &&
                        <legend className="cmp-form-options__legend">
                            {this.props.title}
                        </legend>
                    }
                    {(this.props.type !== "DROP_DOWN" && this.props.type !== "MULTI_DROP_DOWN") ? optionList : null}
                    {(this.props.type === "DROP_DOWN" || this.props.type === "MULTI_DROP_DOWN") &&
                        <>
                            <div className="mb5">{this.props.title}</div>
                            <select className="w-fill" name={this.props.name} id={this.props.id} multiple={this.props.type === "MULTI_DROP_DOWN"}>
                                {this.props.items.map(item =>
                                    <option value={item.value}>{item.text}</option>
                                )}                                
                            </select>
                        </>
                    }

                    {this.props.helpMessage &&
                        <p className="helpText">{this.props.helpMessage}</p>
                    }
                </fieldset>
            </>
        )
    }
}

export default MapTo('wknd-spa-react/components/form/options')(
    Options,
    OptionsEditConfig
);