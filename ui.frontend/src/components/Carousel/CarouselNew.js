import { MapTo } from "@adobe/cq-react-editable-components";
import DOMPurify from "dompurify";
import React, { Component } from "react";
import extractModelId from "../../utils/extract-model-id";

import "./Carousel.scss";
import Teaser from "../Teaser/Teaser";
import Image from "../Image/Image";

const CarouselEditConfig = {
  emptyLabel: "Carousel",

  isEmpty: function (props) {
    return !props || !props.cqItems || props.cqItems.length < 1;
  },
};

class CarouselNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 1,
    };
  }
  componentDidMount() {
    console.log("SlideIndex", this.state.slideIndex);
    this.showSlides(this.state.slideIndex);
  }
  showSlides = (n) => {
    console.log("n", n);

    var i;
    var slides = document.getElementsByClassName("cmp-carousel__item");

    if (n > slides.length) {
      console.log("hi ekdmekdedk");
      console.log("Slides Length", slides.length);

      this.setState({ slideIndex: 1 }, () => {
        console.log("Slide Index", this.state.slideIndex);

        for (i = 0; i < 3; i++) {
          console.log("Surabhi");
          slides[i].style.display = "none";
        }
        console.log("Slides", slides[this.state.slideIndex - 1]);
        slides[this.state.slideIndex - 1].style.display = "block";
      });
    }
    if (n < 1) {
      console.log("hello dkkjckejcekj");
      this.setState({ slideIndex: slides.length }, () => {
        for (i = 0; i < 3; i++) {
          console.log("Surabhi");
          slides[i].style.display = "none";
        }
        console.log("Slides", slides[this.state.slideIndex - 1]);
        slides[this.state.slideIndex - 1].style.display = "block";
      });
    }
    if (n !== 4 && n !== 0) {
      for (i = 0; i < slides.length; i++) {
        console.log("Surabhi");
        slides[i].style.display = "none";
      }
      console.log("Slides", slides[this.state.slideIndex - 1]);
      slides[this.state.slideIndex - 1].style.display = "block";
    }
    // for (i = 0; i < dots.length; i++) {
    //     dots[i].className = dots[i].className.replace(" active", "");
    // }

    // dots[this.state.slideIndex-1].className += " active";
  };

  plusSlides = (e) => {
    console.log("hello1");
    this.setState({ slideIndex: e + this.state.slideIndex }, () => {
      console.log("SlideIndex", this.state.slideIndex);
      this.showSlides(this.state.slideIndex);
    });
  };
  currentSlide = (e) => {
    this.setState({ slideIndex: e }, () => {
      this.showSlides(this.state.slideIndex);
    });
  };

  render() {
    //console.log("Carousel Items", this.props);
    return (
      <>
        <div class="cmp-carousel__content">
          {this.props.cqItemsOrder &&
            this.props.cqItemsOrder.map((listItem, index) => {
              return (
                <CarouselItem
                  item={this.props.cqItemsOrder[index]}
                  items={this.props.cqItems}
                />
              );
            })}
          <a
            class="prev"
            onClick={() => {
              this.plusSlides(-1);
            }}
          >
            &#10094;
          </a>
          <a
            class="next"
            onClick={() => {
              this.plusSlides(1);
            }}
          >
            &#10095;
          </a>

          <br />
          <div style={{ textAlign: "center" }}>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(1);
              }}
            ></span>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(2);
              }}
            ></span>
            <span
              class="dot"
              onClick={() => {
                this.currentSlide(3);
              }}
            ></span>
          </div>
        </div>
      </>
    );
  }
}

class CarouselItem extends Component {
  render() {
    //console.log("Carousel Item", this.props.item);
    //console.log("Image item", this.props.items[this.props.item]);
    let carousel_Item = this.props.items[this.props.item];
    let type = ":type";
    //console.log("Type:", carousel_Item[type]);
    if (carousel_Item[type] == "wknd-spa-react/components/image") {
      return (
        <div class="cmp-carousel__item fade">
          <Image
            src={carousel_Item.src}
            alt={carousel_Item.alt}
            title={carousel_Item.title}
          />
        </div>
      );
    }
    //console.log("Teaser image:", carousel_Item.imagePath);
    return (
      <div class="cmp-carousel__item fade">
        <Teaser
          imagePath={carousel_Item.imagePath}
          alt={carousel_Item.alt}
          title={carousel_Item.title}
          description={carousel_Item.description}
          actions={carousel_Item.actions}
        />
      </div>
    );
  }
}

export default MapTo("wknd-spa-react/components/carousel")(
  CarouselNew,
  CarouselEditConfig
);
