import { MapTo } from '@adobe/cq-react-editable-components';
import DOMPurify from 'dompurify';
import React, { Component } from 'react';

import Slide1 from './aa.jpeg'
import Slide2 from './aa2.jpeg'
import Slide3 from './aa1.jpeg'

import './Carousel.scss';
import Teaser from '../Teaser/Teaser';

const CarouselEditConfig = {
    emptyLabel: 'Carousel',

    isEmpty: function (props) {
        //  return !props || !props.text || props.text.trim().length < 1;
    }
};

class Carousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slideIndex: 1
        }
    }
    componentDidMount() {
        console.log("SlideIndex", this.state.slideIndex)
        this.showSlides(this.state.slideIndex);
    }
    showSlides = (n) => {
        console.log("n", n);

        var i;
        var slides = document.getElementsByClassName("cmp-carousel__item");
        if (n > slides.length) {
            console.log('hi ekdmekdedk')
            console.log("Slides Length", slides.length)

            this.setState({ slideIndex: 1 }, () => {
                console.log("Slide Index", this.state.slideIndex)

                for (i = 0; i < 3; i++) {
                    console.log("Surabhi")
                    slides[i].style.display = "none";
                }
                console.log("Slides", slides[this.state.slideIndex - 1]);
                slides[this.state.slideIndex - 1].style.display = "block";
            });

        }
        if (n < 1) {
            console.log('hello dkkjckejcekj');
            this.setState({ slideIndex: slides.length }, () => {
                for (i = 0; i < 3; i++) {
                    console.log("Surabhi")
                    slides[i].style.display = "none";
                }
                console.log("Slides", slides[this.state.slideIndex - 1]);
                slides[this.state.slideIndex - 1].style.display = "block";
            });



        }
        if (n !== 4 && n !== 0) {

            for (i = 0; i < slides.length; i++) {
                console.log("Surabhi")
                slides[i].style.display = "none";
            }
            console.log("Slides", slides[this.state.slideIndex - 1]);
            slides[this.state.slideIndex - 1].style.display = "block";
        }
        // for (i = 0; i < dots.length; i++) {
        //     dots[i].className = dots[i].className.replace(" active", "");
        // }

        // dots[this.state.slideIndex-1].className += " active";
    }


    plusSlides = (e) => {
        console.log("hello1")
        this.setState({ slideIndex: e + this.state.slideIndex }, () => {
            console.log("SlideIndex", this.state.slideIndex);
            this.showSlides(this.state.slideIndex);
        })
    };
    currentSlide=(e)=>{
        this.setState({slideIndex:e},()=>{
            this.showSlides(this.state.slideIndex);
        })
    }

    render() {
        return (
            <div className="responsivegrid aem-GridColumn aem-GridColumn--default--12" >
                <div className="aem-Grid aem-Grid--12 aem-Grid--default--12">
                    <div className="carousel cmp-carousel--hero aem-GridColumn aem-GridColumn--default--12">
                        <div className="cmp-carousel" role="group" aria-roledescription="carousel" data-cmp-delay="5000">
                            <div className="cmp-carousel__content_container">
                                <div class="cmp-carousel__item fade" role="tabpanel" aria-label="Slide 1 of 3" data-cmp-hook-carousel="item"><div class="teaser cmp-teaser--hero">
                                    <Teaser></Teaser>
                                </div>
                                </div>
                                <div class="cmp-carousel__item fade" role="tabpanel" aria-label="Slide 2 of 3" data-cmp-hook-carousel="item" aria-hidden="true"><div class="teaser cmp-teaser--hero">
                                    <div class="cmp-teaser">
                                        <div class="cmp-teaser__image">
                                            <div data-cmp-src="/content/wknd/us/en/_jcr_content/root/responsivegrid_933431667/carousel/item_1572035298405.coreimg.100{.width}.jpeg/1578602819509/beach-walking.jpeg" data-cmp-widths="100,200,300,400,500,600,700,800,900,1000,1100,1200,1600,1800" data-asset="/content/dam/wknd/en/magazine/san-diego-surf-spots/beach-walking.jpg" data-title="Beach Walking" class="cmp-image" itemscope="" itemtype="http://schema.org/ImageObject">
                                                <img class="cmp-image__image" itemprop="contentUrl" data-cmp-hook-image="image" alt="Young lady with surfboard going to the ocean at surf spot" title="Beach Walking" src={Slide2} />
                                                <meta itemprop="caption" content="Beach Walking" />
                                            </div>
                                        </div>
                                        <div class="cmp-teaser__content" style={{ paddingTop: '2px' }}>
                                            <h2 class="cmp-teaser__title">
                                                <a class="cmp-teaser__title-link" href="/content/wknd/us/en/magazine/san-diego-surf.html">San Diego Surf Spots</a>
                                            </h2>
                                            <div class="cmp-teaser__description">From the hippie beaches of Ocean Beach to the ritzy shores of La Jolla and everywhere in between. Discover the San Diego surf scene.</div>
                                            <div class="cmp-teaser__action-container">
                                                <a class="cmp-teaser__action-link" href="/content/wknd/us/en/magazine/san-diego-surf.html">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="cmp-carousel__item fade" role="tabpanel" aria-label="Slide 3 of 3" data-cmp-hook-carousel="item" aria-hidden="true"><div class="teaser cmp-teaser--hero">
                                    <div class="cmp-teaser">
                                        <div class="cmp-teaser__image">
                                            <div data-cmp-src="/content/wknd/us/en/_jcr_content/root/responsivegrid_933431667/carousel/teaser.coreimg.100{.width}.jpeg/1578602819518/adobestock-185234795.jpeg" data-cmp-widths="100,200,300,400,500,600,700,800,900,1000,1100,1200,1600,1800" data-asset="/content/dam/wknd/en/adventures/downhill-skiing-wyoming/AdobeStock_185234795.jpeg" data-title="Action skiing at the Rolle Pass" class="cmp-image" itemscope="" itemtype="http://schema.org/ImageObject">
                                                <img class="cmp-image__image" itemprop="contentUrl" data-cmp-hook-image="image" alt="A skier does action skiing at the Rolle Pass in the Dolomites, Italy." title="Action skiing at the Rolle Pass" src={Slide3} />
                                                <meta itemprop="caption" content="Action skiing at the Rolle Pass" />
                                            </div>
                                        </div>
                                        <div class="cmp-teaser__content" style={{ paddingTop: '2px' }}>
                                            <h2 class="cmp-teaser__title">
                                                <a class="cmp-teaser__title-link" href="/content/wknd/us/en/adventures/downhill-skiing-wyoming.html">Downhill Skiing Wyoming</a>
                                            </h2>
                                            <div class="cmp-teaser__description"><p>A skiers paradise far from crowds and close to nature with terrain so vast it appears uncharted.</p>
                                            </div>
                                            <div class="cmp-teaser__action-container">
                                                <a class="cmp-teaser__action-link" href="/content/wknd/us/en/adventures/downhill-skiing-wyoming.html">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <a class="prev" onClick={() => { this.plusSlides(-1) }}>&#10094;</a>
                                <a class="next" onClick={() => { this.plusSlides(1) }}>&#10095;</a>
                            </div>
                            <br />
                            <div style={{ textAlign: "center" }}>
                                <span class="dot" onClick={() => { this.currentSlide(1) }}></span>
                                <span class="dot" onClick={() => { this.currentSlide(2) }}></span>
                                <span class="dot" onClick={() => { this.currentSlide(3) }}></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MapTo('wknd-spa-react/components/carousel')(
    Carousel,
    CarouselEditConfig
);
