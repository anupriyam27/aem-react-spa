import { MapTo } from '@adobe/cq-react-editable-components';
import React, { Component } from 'react';

require('./Separator.scss');

const TitleEditConfig = {
    emptyLabel: 'Separator'
};

class Title extends Component {
    render() {
        // console.log('Separator Props', this.props);
        return (
            <div className="m30 w100">
                <hr></hr>
            </div>
        );
    }
}

export default MapTo('wknd-spa-react/components/separator')(
    Title,
    TitleEditConfig
);
